//
//  NewsCell.swift
//  NewsApiDemo
//
//  Created by Kapil Dhawan on 02/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var lblNewsTitle: UILabel!
    
    @IBOutlet weak var lblNewsDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
