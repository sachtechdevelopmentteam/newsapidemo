

import UIKit
struct News: Codable{
    
    var articles: [Article]
  }

struct Article: Codable{
    
    var title: String
    var description: String
    var url: String
    var urlToImage: String
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 
    
    

    @IBOutlet weak var tableNews: UITableView!
    
   var articles = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.tableNews.delegate = self
       self.tableNews.dataSource = self
        fetchNews()
    }
    func fetchNews(){
        
        
        let jsonURL = "https://newsapi.org/v2/top-headlines?sources=the-hindu&apiKey=6afaa696630740688f68b0a9efdec965"
        guard let url = URL(string: jsonURL) else { return }
        
        URLSession.shared.dataTask(with: url) {  (data,response,error) in
            guard let data = data else { return }
           
        
            do{
                let info = try JSONDecoder().decode(News.self, from: data)
                self.articles = info.articles
                print( self.articles )
                DispatchQueue.main.async {
                
                    self.tableNews.reloadData()
                }
            }
            catch let jsonerror{
                print("Error: ", jsonerror)
            }

            }.resume()
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.lblNewsTitle.text = articles[indexPath.row].title
        cell.lblNewsDesc.text = articles[indexPath.row].description
        let imgUrl = articles[indexPath.row].urlToImage
        let imagepath = URL(string: imgUrl)
        do{
        let data = try Data(contentsOf: imagepath!)
        cell.newsImg.image = UIImage(data: data)
        }
        catch {
            print("unable to load!!")
        }
         return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 148
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIApplication.shared.openURL(URL(string: articles[indexPath.row].url)!)
    }

}

